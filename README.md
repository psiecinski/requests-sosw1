# What is it?
It's an simple application for accepting requests from teachers at school.
It's visible now [here](https://zgloszenia.siecinski.ovh/)


## Installation

Use the Node Packaged Module to install

```bash
npm install
```

## How to run
```python
npm start
```

## Used Technologies

- React
- Firebase

## License
[MIT](https://choosealicense.com/licenses/mit/)