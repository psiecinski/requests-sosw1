import React, { Component } from 'react'
import firebase from "./Firebase";
import Logo from "./images/logo.png"
import { Navbar, NavDropdown, Nav, Button, Modal, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Edit from './Edit';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Admin.css';

export default class Admin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            edit: false,
            title: '',
            nr: '',
            building: '',
            message: '',
            cards: [],
            accessCode: '',
            admin: false,
        }
    }
    update = async () => {
        firebase.database().ref("Requests/").on("value", (snapshot) => {
            this.state.cards = [];
            snapshot.forEach(child => {
                let obj = { name: child.key }
                let concatenate = child.val()
                let output = Object.assign({}, obj, concatenate)
                this.setState({ cards: this.state.cards.concat(output) })
                console.log(output)
            })
        }, (errorObject) => {
            console.log("The read failed: " + errorObject.code);
        });
    }
    componentDidMount = async () => {
        this.update()
    }

    inputChangeHandler = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }
    Send = () => {
        let input = { title: this.state.title, nr: this.state.nr, building: this.state.building, message: this.state.message, bg: "light", date: new Date().toISOString().slice(0, 10), accessCode: this.state.accessCode, response: "" }
        firebase.database().ref("Requests/").push(input).then((data) => {
            console.log('data ', data)
            this.handleShow()
            this.update()
        }).catch((error) => {
            console.log('error ', error)
        })
    }
    Change = (event) => {
        firebase.database().ref("Requests/").orderByChild('accessCode').equalTo(event.target.value).on("value", (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                childSnapshot.ref.update({ bg: event.target.name }).then(() => {
                    console.log("Success")
                    window.location.reload(true);
                });
                return true;
            });
        })
    }

    addComment = (event) => {
        let id = event.target.name;
        let mess = this.state[id]
        firebase.database().ref("Requests/").orderByChild('accessCode').equalTo(event.target.id).on("value", (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                childSnapshot.ref.update({ response: mess }).then(() => {
                    console.log("Success")
                    window.location.reload(true);
                });
                return true;
            });
        })
    }
    handleText = (evt) => {
        const value = evt.target.value;
        this.setState({
            ...this.state,
            [evt.target.name]: value
        });
    }
    Delete = (event) => {
        var adaRef = firebase.database().ref('Requests/' + event.target.name);
        adaRef.remove()
            .then(() => {
                console.log("Remove succeeded.")
            })
            .catch((error) => {
                console.log("Remove failed: " + error.message)
            });
        this.update()
        window.location.reload(true);
    }
    render() {
        const cards = this.state.cards.slice(0).reverse().map(card =>
            <Card className="cardAdmin" bg={card.bg}>
                <Card.Header>
                    <a className="date"><b>Data: </b>{card.date}</a>
                    <br />
                    <b>Sala: </b>{card.nr}
                    <br />
                    <div className="text"><b>Budynek: </b>{card.building}
                        <br />
                        <b>Kod dostępu: </b> {card.accessCode}
                        <br />
                        <b>Status: </b>
                        {card.bg == "light"
                            ? <a>Do sprawdzenia</a>
                            : (card.bg == "warning"
                                ? <a>W trakcie</a>
                                : (card.bg == "success"
                                    ? <a>Gotowe</a>
                                    : <div></div>
                                )
                            )
                        }

                    </div>
                </Card.Header>
                <Card.Body>
                    <Card.Title>{card.title}</Card.Title>
                    <Card.Text>
                        {card.message}
                    </Card.Text>
                    <hr />
                    <h5>Odpowiedź Administratora:  </h5>
                    <textarea class="form-control" rows="3" name={card.name} onChange={this.handleText}></textarea>
                    <br />
                    <Button variant="light" value={card.accessCode} name="light" onClick={this.Change}>Nie przejrzane</Button>
                    <Button variant="warning" value={card.accessCode} name="warning" onClick={this.Change}>W trakcie</Button>
                    <Button variant="success" value={card.accessCode} name="success" onClick={this.Change}>Gotowe</Button>
                    <Button variant="danger" value={card.accessCode} name={card.name} onClick={this.Delete}>Delete</Button>
                    <Button variant="info" value={card.accessCode} id={card.accessCode} name={card.name} onClick={this.addComment}>Wyślij</Button>

                </Card.Body>
            </Card>
        );
        return (
            <div>
                {
                    console.log(cards)
                }
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <div className="wrapper">
                        <img src={Logo} width="50" height="50" className="d-inline-block align-top" alt="Logo" />
                        <Navbar.Brand href="/"> <h3 className="logoMiniText navbar-toggler-right">SOSW1 </h3></Navbar.Brand>
                    </div>
                    <Navbar.Brand href="/"><h4 className="logoText navbar-toggler-left" > Zgłoszenia SOSW 1 w Krakowie</h4></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Nav className="mr-auto">
                    </Nav>
                    <Nav.Link><Link to="/"><Button variant="danger">Strona Główna</Button></Link></Nav.Link>
                </Navbar>
                <Modal className="Modal" show={this.state.show} onHide={this.handleShow}>
                    <Modal.Header closeButton>
                        <Modal.Title><input type="text" name="title" required placeholder="Tytuł zgłoszenia" onChange={this.inputChangeHandler} /></Modal.Title>
                    </Modal.Header>
                    <Modal.Body><textarea class="form-control" required name="message" placeholder="Tutaj wpisz treść zgłoszenia" rows="3" onChange={this.inputChangeHandler}></textarea>
                    </Modal.Body>
                    <Modal.Body>
                        <input type="text" name="building" required placeholder="Budynek" onChange={this.inputChangeHandler} />
                        <input type="text" name="nr" required placeholder="Numer sali" onChange={this.inputChangeHandler} className="inputnr" />
                        <hr />
                        <h4>Twój kod do edycji to: <b className="important" >{this.state.accessCode} </b></h4>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleShow}>
                            Wyjdź
          </Button>
                        <Button variant="danger" onClick={this.Send}>
                            Wyślij zgłoszenie
          </Button>
                    </Modal.Footer>
                </Modal>
                <div className="body">
                    {cards}
                </div>

            </div>
        )
    }
}
