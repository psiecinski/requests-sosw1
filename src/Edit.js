import React, { Component } from 'react'
import { Navbar, Button, Modal, Card } from 'react-bootstrap';
import firebase from "./Firebase"
import 'bootstrap/dist/css/bootstrap.min.css';
import './Edit.css';
import './App.css';

export default class Edit extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false,
            allowEdit: false,
            accessCode: "",
            obj: {},
            title: '',
            nr: '',
            building: '',
            message: '',
            accessCode: '',
            id: ''
        }
    }

    handleEdit = () => {
        this.setState({ show: !this.state.show })
    }

    handleExit = () => {
        this.setState({ show: false, allow: false })

    }
    changeState = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    changeTitle = (event) => { //TODO: OPTIMALIZE IT, WTF IS THIS NOW?!
        var remember = event.target.value
        this.setState(prevState => {
            let obj = Object.assign({}, prevState.obj);
            obj.title = remember
            return { obj };
        })
    }
    changeNr = (event) => {
        var remember = event.target.value
        this.setState(prevState => {
            let obj = Object.assign({}, prevState.obj);
            obj.nr = remember
            return { obj };
        })
    }
    changeBuilding = (event) => {
        var remember = event.target.value
        this.setState(prevState => {
            let obj = Object.assign({}, prevState.obj);
            obj.building = remember
            return { obj };
        })
    }
    changeMessage = (event) => {
        var remember = event.target.value
        this.setState(prevState => {
            let obj = Object.assign({}, prevState.obj);
            obj.message = remember
            return { obj };
        })
    }

    Send = () => {
        firebase.database().ref("Requests/").orderByChild('accessCode').equalTo(this.state.accessCode).on("value", (snapshot) => {
            snapshot.forEach((data) => {
                this.setState({ id: data.key, obj: data.val()})
            });
            if (!snapshot.exists()) {
                this.setState({ message: "Nie ma takiego kodu bądź jest on błędny!" })
            } else {
                this.setState({ message: "", allow: true })
            }
        }, (errorObject) => {
            console.log("The read failed: " + errorObject.code);
        })
    }
    handleDelete = () => {
        var adaRef = firebase.database().ref('Requests/' + this.state.id);
        adaRef.remove()
            .then(() => {
                console.log("Remove succeeded.")
            })
            .catch((error) => {
                console.log("Remove failed: " + error.message)
            });
        this.props.update();
        window.location.reload(true);

        this.handleExit();
    }
    Edit = (event) => {
        firebase.database().ref("Requests/").orderByChild('accessCode').equalTo(this.state.accessCode).on("value", (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                childSnapshot.ref.update(this.state.obj).then(() => {
                    console.log("Success")
                });
                return true;
            });
        })
        this.props.update();
        window.location.reload(true);
        this.handleExit();

    }

    render() {
        if (this.state.show) {
            if (this.state.allow) {
                return (
                    <Modal className="Modal" show={this.state.show} onHide={this.handleExit}>
                        <Modal.Header closeButton>
                            <Modal.Title><input type="text input-group-text title" name="title" placeholder="Tytuł zgłoszenia" value={this.state.obj.title} onChange={(e) => this.changeTitle(e)} /></Modal.Title>
                        </Modal.Header>
                        <Modal.Body><textarea class="form-control" name="message" value={this.state.obj.message} placeholder="Tutaj wpisz treść zgłoszenia" rows="3" onChange={(e) => this.changeMessage(e)}></textarea>
                        </Modal.Body>
                        <Modal.Body>
                        <div class="form-group">
                            <input type="text" name="building input-group-text" placeholder="Budynek" value={this.state.obj.building} onChange={(e) => this.changeBuilding(e)} />
                            <input type="text" name="nr input-group-text" placeholder="Numer sali" value={this.state.obj.nr} onChange={(e) => this.changeNr(e)} className="inputnr" />
                        </div>
                            <hr />
                            <h4>Twój kod do edycji to: <b className = "important">{this.state.obj.accessCode}</b></h4>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="danger" onClick={this.handleDelete}>
                                Usuń zgłoszenie
                            </Button>
                            <Button variant="secondary" onClick={this.handleExit}>
                                Wyjdź
                            </Button>
                            <Button variant="danger" onClick={this.Edit}>
                                Wyślij zgłoszenie
                            </Button>
                        </Modal.Footer>
                    </Modal>
                )
            } else {
                return (
                    <Modal className="Modal" show={this.state.show} onHide={this.handleExit}>
                        <Modal.Header closeButton>
                            <h4 className="info">{this.state.message}</h4>
                        </Modal.Header>
                        <Modal.Body>
                            <input type="text" class="form-control" name="accessCode" placeholder="Podaj kod edycji (otrzymany przy dodawaniu zgłoszenia)" onChange={this.changeState} />
                            <br />
                            <Button className="btn-block" variant="danger" onClick={this.Send}>Potwierdź</Button>
                        </Modal.Body>
                    </Modal>
                )
            }
        } else {
            return (
                <div>
                    <Button variant="warning" onClick={this.handleEdit}>Edytuj</Button>
                </div>
            )
        }

    }
}
