import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom';
import PrivateRoute from './routes/PrivateRoutes';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Admin from './Admin';
import Home from './App';

export default class Routing extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Switch>
        <Route exact path='/' component={Home}/>
        <PrivateRoute exact path='/admin' component={Admin}/>
      </Switch>
    )
  }
}
