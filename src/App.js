import React, { Component } from 'react'
import firebase from "./Firebase";
import Logo from "./images/logo.png"
import { Navbar, NavDropdown, Nav, Button, Modal, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Edit from './Edit';
import 'bootstrap/dist/css/bootstrap.min.css';
import Infobar from './Infobar';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show: false,
      edit: false,
      title: '',
      nr: '',
      building: '',
      message: '',
      cards: [],
      accessCode: '',
      admin: false,
    }
  }
  update = async () => {
    console.log("Update!")
    firebase.database().ref("Requests/").on("value", (snapshot) => {
      this.state.cards = [];
      snapshot.forEach(child => {
        this.setState({ cards: this.state.cards.concat(child.val()), name: child.key })
      })
    }, (errorObject) => {
      console.log("The read failed: " + errorObject.code);
    });

  }
  componentDidMount = async () => {
    this.setState({ admin: localStorage.getItem("admin") });
    this.update()
  }
  handleShow = () => {
    this.setState({ show: !this.state.show, accessCode: Math.random().toString(36).substring(7) })
  }
  handleEdit = () => {
    this.setState({ edit: !this.props.edit })
    console.log(this.state)
  }
  inputChangeHandler = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }
  Send = () => {
    let input = { title: this.state.title, nr: this.state.nr, building: this.state.building, message: this.state.message, bg: "light", date: new Date().toISOString().slice(0, 10), accessCode: this.state.accessCode, response: "" }
    firebase.database().ref("Requests/").push(input).then((data) => {
      console.log('data ', data)
      this.handleShow()
      this.update()
    }).catch((error) => {
      console.log('error ', error)
    })
  }

  componentWillUnmount = () => {
    this.state.cards = {}
    this.state.obj = {}
    console.log(this.state)
  }
  render() {
    const cards = this.state.cards.slice(0).reverse().map(card =>
      <Card className="card" bg={card.bg}>
        <Card.Header>        <a className="date"><b>Data: </b>{card.date}</a>
          <br />
          <b>Sala: </b>{card.nr}
          <br />
          <div className="text"><b>Budynek: </b>{card.building} </div>
          <b>Status: </b>

          {card.bg=="light"
        ? <a>Do sprawdzenia</a>
        : (card.bg=="warning"
          ? <a>W trakcie</a>
          : (card.bg=="success"
          ? <a>Gotowe</a>
          : <div></div>
          )
        )
      }
        </Card.Header>
        <Card.Body>
          <Card.Title>{card.title}</Card.Title>
          <Card.Text>
            {card.message}
          </Card.Text>
          <hr />

          {card.bg == "warning" || card.bg == "success" || card.bg == "danger" ? (
            <div><h5>Odpowiedź Administratora:</h5> {
              card.response == "" ? (
                <a>Brak</a>
              ) :
                <a>{card.response}</a>
            }</div>
          ) : (
              <h5></h5>
            )}
            {
              card.bg == "light" ? (
                <h5>W trakcie rozpatrywania</h5>
              ) : (
                <div></div>
              )
            }
        </Card.Body>
      </Card>
    );
    return (
      <div>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <div className="wrapper">
            <img src={Logo} width="50" height="50" className="d-inline-block align-top" alt="Logo" />
            <Navbar.Brand href="/"> <h3 className="logoMiniText navbar-toggler-right">SOSW1 </h3></Navbar.Brand>
          </div>
          <Navbar.Brand href="/"><h4 className="logoText navbar-toggler-left" > Zgłoszenia SOSW 1 w Krakowie</h4></Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
            </Nav>
            <Nav>
              <Nav.Link><Button className="mr-sm-2" variant="danger" onClick={this.handleShow}>Dodaj Zgłoszenie</Button></Nav.Link>
              <Nav.Link eventKey={2}>
                <Edit handleEdit={this.handleEdit} edit={this.state.edit} update={this.update} />
              </Nav.Link>
              {
            this.state.admin == "true" ? (
              <Nav.Link><Link to= "/admin"><Button variant="danger">Admin</Button></Link></Nav.Link>
            ) : (
                <div></div>
              )
          }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
          {
            this.state.show? (
                <Infobar/>
            ) :
            (
              <div></div>
            )
          }
        <Modal className="Modal" show={this.state.show} onHide={this.handleShow}>
          <Modal.Header closeButton>
            <Modal.Title><input type="text" className = "input-group-text title" name="title" required placeholder="Tytuł zgłoszenia" onChange={this.inputChangeHandler} /></Modal.Title>
          </Modal.Header>
          <Modal.Body><textarea class="form-control" required name="message" placeholder="Tutaj wpisz treść zgłoszenia" rows="3" onChange={this.inputChangeHandler}></textarea>
          </Modal.Body>
          <Modal.Body>
          <div class="form-group">
            <input type="text" class = "building input-group-text" name="building" required placeholder="Budynek" onChange={this.inputChangeHandler} />
            <input type="text" class = "nr input-group-text" name="nr" required placeholder="Numer sali" onChange={this.inputChangeHandler} />
          </div>
            <hr />
            <h4>Twój kod do edycji to: <b className = "important" >{this.state.accessCode} </b></h4>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleShow}>
              Wyjdź
          </Button>
            <Button variant="danger" onClick={this.Send}>
              Wyślij zgłoszenie
          </Button>
          </Modal.Footer>
        </Modal>
        <div className="body">
          {cards}
        </div>

      </div>
    )
  }
}
