import React, { useState } from 'react'
import { Toast, Alert, Button } from 'react-bootstrap';
import './Infobar.css';


function Infobar() {
    const [show, setShow] = useState(true);
  
    return (
        <Alert show={show} variant="danger" className = "alertInfo">
          <Alert.Heading>Pamiętaj o dodaniu <b>tytułu</b> w zgłoszeniu oraz zapisaniu swojego <b>kodu dostępu!</b></Alert.Heading>
        </Alert>
    );
  }
  
export default Infobar;